package com.example.demo.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.mappers.ProductMapper;
import com.example.demo.models.dtos.ProductDTO;
import com.example.demo.models.entities.Product;
import com.example.demo.models.forms.ProductForm;
import com.example.demo.repositories.ProductRepository;
import com.example.demo.services.BaseService;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductServiceImpl implements BaseService<ProductDTO, ProductForm, Long> {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public List<ProductDTO> getAll()
    {
        return this.productRepository.findAll()
                    .stream()
                    .map(this.productMapper::toDto)
                    .collect(Collectors.toList());
    }

    public ProductDTO getOneById(Long id)
    {
        return this.productMapper.toDto(this.productRepository.findById(id).orElse(null));
    }

    public void insert(ProductForm form)
    {
        Product p = this.productMapper.formToEntity(form);
        this.productRepository.save(p);
    }

    @Override
    public void delete(Long id) {
        Product p = this.productRepository.findById(id).orElse(null);
        
        this.productRepository.delete(p);        
    }

    @Override
    public ProductDTO update(ProductForm form, Long id) {
        Product p = this.productRepository.findById(id).orElse(null);
        
        p.setCategory(form.getCategory());
        p.setName(form.getName());
        p.setPrice(form.getPrice());
        this.productRepository.save(p);

        return this.productMapper.toDto(p);
    }
}
