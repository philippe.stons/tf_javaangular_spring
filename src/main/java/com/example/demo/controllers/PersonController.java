package com.example.demo.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.example.demo.models.dtos.PersonDTO;
import com.example.demo.models.forms.PersonForm;
import com.example.demo.services.impl.PersonServiceImpl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(path = "/person")
public class PersonController {

    private final PersonServiceImpl personService;

    public PersonController(PersonServiceImpl personService) {
        this.personService = personService;
    }

    @GetMapping("")
    public ModelAndView getTest()
    {
        Map<String, Object> datas = new HashMap<String, Object>(); 
        // datas.put("test", "Spring c'est super!!");
        // datas.put("boolVar", false);
        // datas.put("type", 'D');

        // List<Integer> list = new ArrayList<>();
        // list.add(5);
        // list.add(2);
        // list.add(3);
        // list.add(42);
        // datas.put("list", list);

        List<PersonDTO> persons = this.personService.getAll();
        datas.put("persons", persons);

        return new ModelAndView("person/list", datas);
    }

    @GetMapping("add")
    public String getAddPerson(Model model)
    {
        PersonForm form = new PersonForm();
        model.addAttribute("personForm", form);

        return "person/add";
    }

    @PostMapping("add/")
    public String postAddPerson(Model model, @Valid PersonForm personForm, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {            
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "addPerson";
        }
        
        // Person p = new Person();
        // p.setFirstname(personForm.getFirstname());
        // p.setLastname(personForm.getLastname());
        // this.personService.insert(p);
        this.personService.insert(personForm);

        return "redirect:/person/";        
    }

    @GetMapping("edit/{id}")
    public String getAddPerson(Model model, @PathVariable(name = "id") Long id)
    {
        PersonDTO p = this.personService.getOneById(id);
        PersonForm form = new PersonForm();
        if(p != null)
        {
            form.setFirstname(p.getFirstname());
            form.setLastname(p.getLastname());
        }
        model.addAttribute("personForm", form);
        model.addAttribute("id", id);
        model.addAttribute("edit", true);

        return "person/add";
    }

    @PostMapping("edit/{id}")
    public String postAddPerson(Model model, @Valid PersonForm personForm, BindingResult bindingResult,
        @PathVariable(name = "id") Long id)
    {
        if(bindingResult.hasErrors())
        {
            List<ObjectError> test = bindingResult.getAllErrors();
            
            model.addAttribute("errors", test);
            return "person/add";
        }
        
        // Person p = new Person();
        // p.setFirstname(personForm.getFirstname());
        // p.setLastname(personForm.getLastname());
        // this.personService.insert(p);
        this.personService.update(personForm, id);

        return "redirect:/person/";        
    }

    @GetMapping("delete/{id}")
    public String deletePerson(Model model, @PathVariable(name = "id") Long id)
    {
        this.personService.delete(id);
        return "redirect:/person/";
    }
}
