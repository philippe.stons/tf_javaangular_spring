package com.example.demo.controllers;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.models.dtos.ProductDTO;
import com.example.demo.models.forms.ProductForm;
import com.example.demo.services.impl.ProductServiceImpl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/product/")
public class ProductController {
    private final ProductServiceImpl productService;

    public ProductController(ProductServiceImpl productService) {
        this.productService = productService;
    }

    @GetMapping("")
    public String getProducts(Model model)
    {
        List<ProductDTO> products = this.productService.getAll();
        model.addAttribute("products", products);

        return "product/list";
    }

    @GetMapping("add")
    public String getAddProduct(Model model)
    {
        ProductForm form = new ProductForm();
        model.addAttribute("productForm", form);

        return "product/add";
    }

    @PostMapping("add")
    public String addProduct(Model model, @Valid ProductForm form, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            model.addAttribute("errors", bindingResult.getAllErrors());
        }

        this.productService.insert(form);

        return "redirect:/product/";
    }

    @GetMapping("edit/{id}")
    public String getEditProduct(Model model, @PathVariable(name = "id") Long id)
    {
        ProductDTO product = this.productService.getOneById(id);
        ProductForm form = new ProductForm();
        if(product != null) 
        {
            form.setCategory(product.getCategory());
            form.setName(product.getName());
            form.setPrice(product.getPrice());
        }
        model.addAttribute("productForm", form);
        model.addAttribute("edit", true);
        model.addAttribute("id", id);

        return "product/add";
    }

    @PostMapping("edit/{id}")
    public String editProduct(Model model, @Valid ProductForm form, BindingResult bindingResult,
        @PathVariable(name = "id") Long id)
    {
        if(bindingResult.hasErrors() || id < 1)
        {
            model.addAttribute("errors", bindingResult.getAllErrors());
        }

        this.productService.update(form, id);

        return "redirect:/product/";
    }
}
